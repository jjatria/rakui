unit package Rakui;

constant COLOR_CLEAR        is export(:style)  = -1;
constant COLOR_BLACK        is export(:style)  = 0;
constant COLOR_RED          is export(:style)  = 1;
constant COLOR_GREEN        is export(:style)  = 2;
constant COLOR_YELLOW       is export(:style)  = 3;
constant COLOR_BLUE         is export(:style)  = 4;
constant COLOR_MAGENTA      is export(:style)  = 5;
constant COLOR_CYAN         is export(:style)  = 6;
constant COLOR_WHITE        is export(:style)  = 7;

constant MODIFIER_CLEAR     is export(:style)  = 0;
constant MODIFIER_BOLD      is export(:style)  = 1 +< 8;
constant MODIFIER_UNDERLINE is export(:style)  = 1 +< 9;
constant MODIFIER_REVERSE   is export(:style)  = 1 +< 10;

# Symbols

constant TOP_LEFT           is export(:symbol) = '┌';
constant TOP_RIGHT          is export(:symbol) = '┐';
constant BOTTOM_LEFT        is export(:symbol) = '└';
constant BOTTOM_RIGHT       is export(:symbol) = '┘';

constant VERTICAL_LINE      is export(:symbol) = '│';
constant HORIZONTAL_LINE    is export(:symbol) = '─';

constant VERTICAL_LEFT      is export(:symbol) = '┤';
constant VERTICAL_RIGHT     is export(:symbol) = '├';
constant HORIZONTAL_UP      is export(:symbol) = '┴';
constant HORIZONTAL_DOWN    is export(:symbol) = '┬';

constant QUOTA_LEFT         is export(:symbol) = '«';
constant QUOTA_RIGHT        is export(:symbol) = '»';

constant VERTICAL_DASH      is export(:symbol) = '┊';
constant HORIZONTAL_DASH    is export(:symbol) = '┈';

constant DOT                is export(:symbol) = '•';
constant ELLIPSES           is export(:symbol) = '…';
constant UP_ARROW           is export(:symbol) = '▲';
constant DOWN_ARROW         is export(:symbol) = '▼';
constant COLLAPSED          is export(:symbol) = '+';
constant EXPANDED           is export(:symbol) = '−';

constant BARS               is export(:symbol) = ( ' ', '▁', '▂', '▃', '▄', '▅', '▆', '▇', '█' );
constant SHADED_BLOCKS      is export(:symbol) = ( ' ', '░', '▒', '▓', '█' );
constant IRREGULAR_BLOCKS   is export(:symbol) = (
    ' ', '▘', '▝', '▀', '▖', '▌', '▞', '▛',
    '▗', '▚', '▐', '▜', '▄', '▙', '▟', '█',
);
