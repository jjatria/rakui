unit class Rakui::Buffer;

use Rakui::Constants :style;
use Rakui::Style;
use Rakui::Geometry;
use Rakui::Cell;

has Rakui::Rectangle $.rectangle is required;
has Rakui::Cell %!cell-map{Rakui::Point};

method TWEAK () {
    self.fill:
        Rakui::Cell.new( rune => ' ', style => STYLE_CLEAR ),
        $!rectangle;
}

multi method cell ( Int:D $x, Int:D $y --> Rakui::Cell ) {
    return-rw %!cell-map{ Rakui::Point.new: $x, $y };
}

multi method cell ( Rakui::Point:D $p --> Rakui::Cell ) {
    return-rw %!cell-map{$p};
}

method cells { %!cell-map.pairs }

method fill ( Rakui::Cell:D $c, Rakui::Rectangle:D $r ) {
    for $r.min.x .. $r.max.x -> $x {
        for $r.min.y .. $r.max.y -> $y {
            %!cell-map{ Rakui::Point.new: $x, $y } = $c;
        }
    }
}

multi method set-string (
    Str:D          $string,
    Rakui::Style:D $style,
    Rakui::Point:D $p,
) {
    my $x = 0;
    for $string.comb -> $rune {
        my $i = Rakui::Point.new: $p.x + $x, $p.y;
        %!cell-map{$i} = Rakui::Cell.new: :$rune, :$style;
        $x++;
    }
}
