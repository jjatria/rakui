unit package Rakui;

use Rakui::Style;

grammar Style::Grammar {
    token TOP { [ <style> || <plain> ]+ }

    token plain { <text> }

    token style { '[' <text> ']' '(' <pair>+ % ',' ')' }

    token text {
        [
        | <-[ \[ \] ]>
        | <after \\> <[ \[ \] ]>
        ]+
    }

    token pair {
        | <key=.color-key>    ':' <value=.color-value>
        | <key=.modifier-key> ':' <value=.modifier-value>
    }

    proto token color-key                     {*}
          token color-key:sym<fg>             { 'fg'        }
          token color-key:sym<bg>             { 'bg'        }

    proto token modifier-key                  {*}
          token modifier-key:sym<mod>         { 'mod'       }

    proto token color-value                   {*}
          token color-value:sym<red>          { 'red'       }
          token color-value:sym<blue>         { 'blue'      }
          token color-value:sym<black>        { 'black'     }
          token color-value:sym<cyan>         { 'cyan'      }
          token color-value:sym<yellow>       { 'yellow'    }
          token color-value:sym<white>        { 'white'     }
          token color-value:sym<clear>        { 'clear'     }
          token color-value:sym<green>        { 'green'     }
          token color-value:sym<magenta>      { 'magenta'   }

    proto token modifier-value                {*}
          token modifier-value:sym<bold>      { 'bold'      }
          token modifier-value:sym<clear>     { 'clear'     }
          token modifier-value:sym<reverse>   { 'reverse'   }
          token modifier-value:sym<underline> { 'underline' }
}

class Style::Actions {
    use Rakui::Constants :style;
    use Rakui::Cell;

    has $.default-style is required;

    method TOP ($/) { make $/.chunks».value».made.flat }

    method plain ($/) {
        make $/.Str.comb.map: -> $rune {
            Rakui::Cell.new: :$rune, style => $!default-style;
        };
    }

    method style ($/) {
        my $style = Rakui::Style.new: |%( $<pair>».made );
        make $<text>.Str.comb.map: -> $rune {
            Rakui::Cell.new: :$rune, :$style;
        };
    }

    method pair ($/) { make $<key>.made => $<value>.made }

    method text ($/) { make ~$/ }

    method color-key:sym<fg>             ($/) { make 'fg'          }
    method color-key:sym<bg>             ($/) { make 'bg'          }
    method modifier-key:sym<mod>         ($/) { make 'modifier'    }

    method color-value:sym<clear>        ($/) { make COLOR_CLEAR   }
    method color-value:sym<black>        ($/) { make COLOR_BLACK   }
    method color-value:sym<red>          ($/) { make COLOR_RED     }
    method color-value:sym<green>        ($/) { make COLOR_GREEN   }
    method color-value:sym<yellow>       ($/) { make COLOR_YELLOW  }
    method color-value:sym<blue>         ($/) { make COLOR_BLUE    }
    method color-value:sym<magenta>      ($/) { make COLOR_MAGENTA }
    method color-value:sym<white>        ($/) { make COLOR_WHITE   }

    method modifier-value:sym<bold>      ($/) { make MODIFIER_BOLD      }
    method modifier-value:sym<clear>     ($/) { make MODIFIER_CLEAR     }
    method modifier-value:sym<reverse>   ($/) { make MODIFIER_REVERSE   }
    method modifier-value:sym<underline> ($/) { make MODIFIER_UNDERLINE }
}

our sub parse-styles ( Str:D $text, Rakui::Style:D $default-style ) {
    my $actions = Rakui::Style::Actions.new: :$default-style;
    Rakui::Style::Grammar.parse( $text, :$actions ).made;
}
