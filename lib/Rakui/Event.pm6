unit package Rakui;

constant EVENT_KEYBOARD is export = 0;
constant EVENT_MOUSE    is export = 1;
constant EVENT_RESIZE   is export = 2;

class Event {
    has Int $.type;
    has Str $.id;
}

class Event::Keyboard is Event { }

class Event::Mouse is Event {
    has $.x;
    has $.y;
    has Bool $.drag;
}

class Event::Resize is Event {
    has $.width;
    has $.height;
}
