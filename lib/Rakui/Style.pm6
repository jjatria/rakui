unit package Rakui;

use Rakui::Constants :style;

subset Color of Int where -1 <= * <= 255;

class Style {
    my subset Modifier of UInt;

    has Color $.fg          = COLOR_CLEAR;
    has Color $.bg          = COLOR_CLEAR;
    has Modifier $.modifier = MODIFIER_CLEAR;

    multi method new ( Color $fg )            { self.bless: :$fg }
    multi method new ( Color $fg, Color $bg ) { self.bless: :$fg, :$bg }

    multi method new ( Color $fg, Color $bg, Modifier $modifier ) {
        self.bless: :$fg, :$bg, :$modifier;
    }
}

constant STYLE_CLEAR is export = Style.new(
    fg       => COLOR_CLEAR,
    bg       => COLOR_CLEAR,
    modifier => MODIFIER_CLEAR,
);
