unit class Rakui::Cell;

use Rakui::Style;

has Str $.rune where *.chars == 1;
has Rakui::Style $.style;

multi method new ( Str:D $rune, Rakui::Style:D $style --> Rakui::Cell) {
    self.bless: :$rune, :$style;
}
