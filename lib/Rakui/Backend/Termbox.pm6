unit package Rakui;

use Termbox :ALL;
use Rakui::Event;
use Rakui::Geometry;
use Rakui::Cell;

constant %keyboard-map = (
    Pair.new( TB_KEY_F1,          '<F1>' ),
    Pair.new( TB_KEY_F2,          '<F2>' ),
    Pair.new( TB_KEY_F3,          '<F3>' ),
    Pair.new( TB_KEY_F4,          '<F4>' ),
    Pair.new( TB_KEY_F5,          '<F5>' ),
    Pair.new( TB_KEY_F6,          '<F6>' ),
    Pair.new( TB_KEY_F7,          '<F7>' ),
    Pair.new( TB_KEY_F8,          '<F8>' ),
    Pair.new( TB_KEY_F9,          '<F9>' ),
    Pair.new( TB_KEY_F10,         '<F10>' ),
    Pair.new( TB_KEY_F11,         '<F11>' ),
    Pair.new( TB_KEY_F12,         '<F12>' ),
    Pair.new( TB_KEY_INSERT,      '<Insert>' ),
    Pair.new( TB_KEY_DELETE,      '<Delete>' ),
    Pair.new( TB_KEY_HOME,        '<Home>' ),
    Pair.new( TB_KEY_END,         '<End>' ),
    Pair.new( TB_KEY_PGUP,        '<PageUp>' ),
    Pair.new( TB_KEY_PGDN,        '<PageDown>' ),
    Pair.new( TB_KEY_ARROW_UP,    '<Up>' ),
    Pair.new( TB_KEY_ARROW_DOWN,  '<Down>' ),
    Pair.new( TB_KEY_ARROW_LEFT,  '<Left>' ),
    Pair.new( TB_KEY_ARROW_RIGHT, '<Right>' ),

    Pair.new( TB_KEY_CTRL_TILDE,  '<C-<Space>>' ), # tb.KeyCtrl2 tb.KeyCtrlTilde
    Pair.new( TB_KEY_CTRL_A,      '<C-a>' ),
    Pair.new( TB_KEY_CTRL_B,      '<C-b>' ),
    Pair.new( TB_KEY_CTRL_C,      '<C-c>' ),
    Pair.new( TB_KEY_CTRL_D,      '<C-d>' ),
    Pair.new( TB_KEY_CTRL_E,      '<C-e>' ),
    Pair.new( TB_KEY_CTRL_F,      '<C-f>' ),
    Pair.new( TB_KEY_CTRL_G,      '<C-g>' ),
    Pair.new( TB_KEY_BACKSPACE,   '<C-<Backspace>>' ), # tb.KeyCtrlH
    Pair.new( TB_KEY_TAB,         '<Tab>' ),           # tb.KeyCtrlI
    Pair.new( TB_KEY_CTRL_I,      '<C-i>' ),
    Pair.new( TB_KEY_CTRL_J,      '<C-j>' ),
    Pair.new( TB_KEY_CTRL_K,      '<C-k>' ),
    Pair.new( TB_KEY_CTRL_L,      '<C-l>' ),
    Pair.new( TB_KEY_ENTER,       '<Enter>' ), # tb.KeyCtrlM
    Pair.new( TB_KEY_CTRL_N,      '<C-n>' ),
    Pair.new( TB_KEY_CTRL_O,      '<C-o>' ),
    Pair.new( TB_KEY_CTRL_P,      '<C-p>' ),
    Pair.new( TB_KEY_CTRL_Q,      '<C-q>' ),
    Pair.new( TB_KEY_CTRL_R,      '<C-r>' ),
    Pair.new( TB_KEY_CTRL_S,      '<C-s>' ),
    Pair.new( TB_KEY_CTRL_T,      '<C-t>' ),
    Pair.new( TB_KEY_CTRL_U,      '<C-u>' ),
    Pair.new( TB_KEY_CTRL_V,      '<C-v>' ),
    Pair.new( TB_KEY_CTRL_W,      '<C-w>' ),
    Pair.new( TB_KEY_CTRL_X,      '<C-x>' ),
    Pair.new( TB_KEY_CTRL_Y,      '<C-y>' ),
    Pair.new( TB_KEY_CTRL_Z,      '<C-z>' ),
    Pair.new( TB_KEY_ESC,         '<Escape>' ), # tb.KeyCtrlLsqBracket tb.KeyCtrl3
    Pair.new( TB_KEY_CTRL_4,      '<C-4>' ),    # tb.KeyCtrlBackslash
    Pair.new( TB_KEY_CTRL_5,      '<C-5>' ),    # tb.KeyCtrlRsqBracket
    Pair.new( TB_KEY_CTRL_6,      '<C-6>' ),
    Pair.new( TB_KEY_CTRL_7,      '<C-7>' ), # tb.KeyCtrlSlash tb.KeyCtrlUnderscore
    Pair.new( TB_KEY_SPACE,       '<Space>' ),
    Pair.new( TB_KEY_BACKSPACE2,  '<Backspace>' ), # tb.KeyCtrl8

);

constant %mouse-button-map = (
    Pair.new( TB_KEY_MOUSE_LEFT,       '<MouseLeft>' ),
    Pair.new( TB_KEY_MOUSE_RIGHT,      '<MouseRight>' ),
    Pair.new( TB_KEY_MOUSE_MIDDLE,     '<MouseMiddle>' ),
    Pair.new( TB_KEY_MOUSE_RELEASE,    '<MouseRelease>' ),
    Pair.new( TB_KEY_MOUSE_WHEEL_UP,   '<MouseWheelUp>' ),
    Pair.new( TB_KEY_MOUSE_WHEEL_DOWN, '<MouseWheelDown>' ),
);

sub convert-event ( Termbox::Event:D $in --> Rakui::Event ) {
    die $in.type if $in.type < 0;

    given $in.type {
        when TB_EVENT_KEY {
            my $id = $in.mod == TB_MOD_ALT ?? '<M-%s>' !! '%s';

            if $in.ch != 0 {
                # TODO: This needs to be properly decoded
                $id = sprintf( $id, Blob.new( $in.ch.Int ).decode );
            } else {
                $id = sprintf( $id, %keyboard-map{ $in.key } // '' );
            }

            Rakui::Event::Keyboard.new: type => EVENT_KEYBOARD, id => $id;

        }
        when TB_EVENT_MOUSE {
            Rakui::Event::Mouse.new:
                id   => %mouse-button-map{ $in.key } // 'Unknown_Mouse_Button',
                type => EVENT_MOUSE,
                x    => $in.x,
                y    => $in.y,
                drag => $in.mod == TB_MOD_MOTION,
        }
        when TB_EVENT_RESIZE {
            Rakui::Event::Resize.new:
                id     => '<Resize>',
                type   => EVENT_RESIZE,
                width  => $in.w,
                height => $in.h;
        }
    }
}

our sub init () {
    if tb-init() {
        die 'Could not initialise Termbox';
    }

    tb-select-input-mode( TB_INPUT_ESC +| TB_INPUT_MOUSE );
    tb-select-output-mode( TB_OUTPUT_256 );

    return;
}

our sub refresh () { tb-present }

our sub close () { tb-shutdown }

our sub terminal-size ( --> Rakui::Point ) {
    tb-present;

    my $x  = tb-width;
    my $y = tb-height;

    Rakui::Point.new: :$x, :$y;
}

our sub clear { tb-clear }

our sub terminal-area ( --> Rakui::Rectangle ) {
    Rakui::Rectangle.new: Rakui::Point.zero, terminal-size;
}

our sub put-cell ( Rakui::Point:D $point, Rakui::Cell:D $cell ) {
    tb-utf8-char-to-unicode( my uint32 $char, $cell.rune );

    tb-change-cell(
        $point.x,
        $point.y,
        $char,
        ( $cell.style.fg + 1 ) +| $cell.style.modifier,
        $cell.style.bg + 1,
    );
}

our sub poll-events () {
    my $events = Supplier.new;

    start {
        while tb-poll-event( my $ev = Termbox::Event.new ) {
            $events.emit: convert-event($ev);
        }
    }

    $events.Supply;
}
