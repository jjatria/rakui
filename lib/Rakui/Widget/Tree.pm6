unit package Rakui::Widget;

use Rakui;
use Rakui::Block;

my constant INDENT = '  ';

class Tree::Node {
    has $.value; #    fmt.Stringer
    has Tree::Node @.nodes;
    has Bool $.expanded is rw;
    has Int  $.level    is rw;

    method parse-styles ( Rakui::Style:D $style ) {
        my $string = @!nodes
            ?? INDENT x $.level
                ~ ( $!expanded ?? Rakui::EXPANDED !! Rakui::COLLAPSED )
                ~ ' '
            !! INDENT x $.level + 1;

        $string ~= $!value;

        return Rakui::parse-styles( $string, $style );
    }
}

class Tree is Rakui::Block {
    has Int $.top-row      where * >= 0 = 0;
    has Int $.selected-row where * >= 0 = 0;
    has Rakui::Style $.selected-row-style .= new: Rakui::COLOR_WHITE;
    has Tree::Node @.rows;  # Flattened nodes for rendering
    has Tree::Node @.nodes;

    has Rakui::Style $.text-style .= new: Rakui::COLOR_WHITE;
    has Bool $.wrap-text = True;

    method TWEAK { self!prepare-nodes }

    method set-nodes ( Tree::Node @nodes ) {
        @!nodes = @nodes;
        self!prepare-nodes;
    }

    method !prepare-nodes {
        @!rows = ();
        self!prepare-node( $_, 0 ) for @!nodes;
    }

    method !prepare-node ( Tree::Node:D $node, Int:D $level ) {
        @!rows.push: $node;
        $node.level = $level;

        if $node.expanded {
            samewith( self, $_, $level + 1 ) for $node.nodes;
        }
    }

    # &code is a function used for walking a Tree.
    # To interrupt the walking process function should return false.
    method walk ( &code ) {
        for @!nodes -> $node {
            last unless self!walk( $node, &code );
        }
    }

    method !walk ( Tree::Node $node, &code --> Bool ) {
        return False unless &code($node);

        for $node.nodes -> $n {
            return False unless self!walk( $n, &code );
        }

        return True
    }

    method draw ( Rakui::Buffer $buffer ) {
        use Terminal::WCWidth;

        my $point = $.inner.min;

        # Adjusts view into widget
        if $!selected-row >= $.inner.dy + $!top-row {
            $!top-row = $!selected-row - $.inner.dy + 1;
        }
        elsif $!selected-row < $!top-row {
            $!top-row = $!selected-row;
        }

        # Draw rows
        for $!top-row ..^ @!rows.elems -> $row {
            last if $point.y >= $.inner.max.y;

            my Rakui::Cell @cells = @!rows[$row].parse-styles( $!text-style );

            @cells = Rakui::wrap-cells( @cells, $.inner.dx ) if $!wrap-text;

            for @cells -> $cell {
                last if $point.y >= $.inner.max.y;

                my $style = $row == $!selected-row
                    ?? $!selected-row-style !! $cell.style;

                if $point.x + 1 == $.inner.max.x + 1 && @cells.elems > $.inner.dx {
                    $buffer.cell( $point.sub( x => 1 ) )
                        = Rakui::Cell.new: rune => Rakui::ELLIPSES, :$style;
                    last;
                }
                else {
                    $buffer.cell($point)
                        = Rakui::Cell.new: rune => $cell.rune, :$style;

                    $point = $point.add( x => 1 );
                }
            }

            $point = Rakui::Point.new: $.inner.min.x, $point.y + 1;
        }

        # Draw UP_ARROW if needed
        if $!top-row > 0 {
            # If the row has wide characters, then we need to adjust the
            # arrow position to fix the alignment
            my $row-at-top = @!rows[$!top-row];

            my $wide = $row-at-top.chars != wcswidth($row-at-top)
                ?? wcswidth($row-at-top) !! 0;

            $buffer.cell( $.inner.max.x - 1 - $wide, $.inner.min.y )
                = Rakui::Cell.new:
                    rune  => Rakui::UP_ARROW,
                    style => Rakui::Style.new( Rakui::COLOR_WHITE );
        }

        # Draw DOWN_ARROW if needed
        if @!rows.elems > $!top-row.Int + $.inner.dy {
            $buffer.cell( $.inner.max.x - 1, $.inner.max.y - 1 )
                = Rakui::Cell.new:
                    rune  => Rakui::DOWN_ARROW,
                    style => Rakui::Style.new( Rakui::COLOR_WHITE );
        }

        callsame;
    }

    method scroll-amount ( Int:D $amount ) {
        return unless @!rows;

        if @!rows.elems - $!selected-row <= $amount {
            $!selected-row = @!rows.elems - 1;
        }
        elsif $!selected-row + $amount < 0 {
            $!selected-row = 0;
        }
        else {
            $!selected-row += $amount;
        }
    }

    method scroll-up     { self.scroll-amount: -1 }
    method scroll-down   { self.scroll-amount:  1 }
    method scroll-top    { $!selected-row = 0 }
    method scroll-bottom { $!selected-row = @!rows.elems - 1 }

    method scroll-page-up {
        # If an item is selected below top row, then go to the top row.
        if $!selected-row > $!top-row {
            $!selected-row = $!top-row;
        }
        else {
            self.scroll-amount: -$.inner.dy;
        }
    }

    method scroll-page-down { self.scroll-amount: $.inner.dy }

    method scroll-half-page-up {
        self.scroll-amount: -( $.inner.dy / 2 ).Int;
    }

    method scroll-half-page-down {
        self.scroll-amount: ( $.inner.dy / 2 ).Int;
    }

    method selected-node ( --> Tree::Node ) {
        @!rows ?? @!rows[$!selected-row] !! Tree::Node;
    }

    method collapse {
        @!rows[$!selected-row].expanded = False;
        self!prepare-nodes;
    }

    method expand {
        .expanded = True if .nodes given @!rows[$!selected-row];
        self!prepare-nodes;
    }

    method toggle-expand {
        .expanded = !.expanded if .nodes given @!rows[$!selected-row];
        self!prepare-nodes;
    }

    method expand-all {
        self.walk: -> $node {
            $node.expanded = True if $node.nodes;
            True;
        };
        self!prepare-nodes;
    }

    method collapse-all {
        self.walk: -> $node {
            $node.expanded = False if $node.nodes;
            True;
        };
        self!prepare-nodes;
    }
}
