unit package Rakui::Widget;

use Rakui;
use Rakui::Block;

class BarChart is Rakui::Block {
    my @colors = (
        Rakui::COLOR_RED,
        Rakui::COLOR_GREEN,
        Rakui::COLOR_YELLOW,
        Rakui::COLOR_BLUE,
        Rakui::COLOR_MAGENTA,
        Rakui::COLOR_CYAN,
        Rakui::COLOR_WHITE,
    );

    my @styles = @colors.map: { Rakui::Style.new: $^color };

    has Rakui::Color @.bar-colors   = @colors;
    has Rakui::Style @.label-styles = @styles;
    has Rakui::Style @.num-styles   = @styles; # Only fg and modifier are used

    has &.num-formatter = *.Str;

    has @.data;
    has Str @.labels;

    has Int $.bar-gap   = 1;
    has Int $.bar-width = 3;
    has Int $.max-val   = 0;

    sub pick-from ( @items, $i ) { @items[ $i % @items.elems ] }

    method draw (Rakui::Buffer:D $buffer ) {
        use Terminal::WCWidth;

        callsame;

        my $max-val = $!max-val ?? $!max-val !! @!data.max;

        my $bar-x = $.inner.min.x;

        for @!data.kv -> $i, $data {
            # Draw bar
            my $height = ( $data / $max-val * ( $.inner.dy - 1 ) ).Int;

            for $bar-x ..^ ( $bar-x + $!bar-width, $.inner.max.x ).min -> $x {
                for $.inner.max.y - 2 ...^ $.inner.max.y - 2 - $height -> $y {
                    $buffer.cell( $x, $y ) = Rakui::Cell.new(
                        rune  => ' ',
                        style => Rakui::Style.new(
                            Rakui::COLOR_CLEAR,
                            @!bar-colors.&pick-from($i),
                        ),
                    );
                }
            }

            # Draw label
            if $i < @!labels.elems {
                my $label-x = $bar-x
                    + ( $!bar-width / 2 ).Int
                    - ( wcswidth( @!labels[$i] ) / 2 ).Int;

                $buffer.set-string(
                    @!labels[$i],
                    @!label-styles.&pick-from($i),
                    Rakui::Point.new( $label-x, $.inner.max.y - 1 ),
                );
            }

            # Draw number
            my $number-x = $bar-x + ( $!bar-width / 2 ).Int;
            if $number-x <= $.inner.max.x {
                $buffer.set-string(
                    &!num-formatter($data),
                    Rakui::Style.new(
                        @!num-styles.&pick-from( $i + 1 ).fg,
                        @!bar-colors.&pick-from( $i ),
                        @!num-styles.&pick-from( $i + 1 ).modifier,
                    ),
                    Rakui::Point.new( $number-x, $.inner.max.y - 2 ),
                )
            }

            $bar-x += $!bar-width + $!bar-gap;
        }
    }
}
