unit package Rakui::Widget;

use Rakui;
use Rakui::Block;
use Rakui::Style;

class Gauge is Rakui::Block {
    has Int          $.percent is rw;
    has Str          $.label is rw = '';
    has Rakui::Style $.label-style .= new: Rakui::COLOR_WHITE;
    has Rakui::Color $.bar-color    =      Rakui::COLOR_WHITE;

    method draw ( Rakui::Buffer:D $buffer ) {
        callsame;

        my $label = $!label ?? $!label !! sprintf( '%d%%', $!percent );

        # Plot bar
        my $bar-width = ( $!percent / 100 * $.inner.dx ).Int;

        $buffer.fill(
            Rakui::Cell.new(
                rune => ' ',
                style => Rakui::Style.new: Rakui::COLOR_CLEAR, $!bar-color,
            ),
            Rakui::Rectangle.new(
                $.inner.min.x,
                $.inner.min.y,
                $.inner.min.x - 1 + $bar-width,
                $.inner.max.y - 1,
            ),
        );

        # Plot label

        my $x = (
            $.inner.min.x
            + $.inner.dx   / 2
            - $label.chars / 2
        ).Int;

        my $y = ( $.inner.min.y + ( $.inner.dy - 1 ) / 2 ).Int;

        if $y < $.inner.max.y {
            my $reverse = Rakui::Style.new(
                $!bar-color,
                Rakui::COLOR_CLEAR,
                Rakui::MODIFIER_REVERSE,
            );

            for $label.comb.kv -> $i, $char {
                my $style = $x + $i + 1 <= $.inner.min.x + $bar-width
                    ?? $reverse !! $!label-style;

                $buffer.cell( $x + $i + 1, $y )
                    = Rakui::Cell.new: rune => ~$char, :$style;
            }
        }
    }
}
