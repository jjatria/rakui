unit package Rakui::Widget;

use Rakui;
use Rakui::Block;

class List is Rakui::Block {
    has Str @.rows;
    has Bool $.wrap-text;
    has Int $.selected-row = 0;
    has Int $.top-row      = 0;
    has Rakui::Style $.text-style         .= new: Rakui::COLOR_WHITE;
    has Rakui::Style $.selected-row-style .= new: Rakui::COLOR_WHITE;

    method draw ( Rakui::Buffer:D $buffer ) {
        use Terminal::WCWidth;

        my $point = $.inner.min;

        # Adjusts view into widget
        if $!selected-row >= $.inner.dy + $!top-row {
            $!top-row = $!selected-row - $.inner.dy + 1;
        }
        elsif $!selected-row < $!top-row {
            $!top-row = $!selected-row;
        }

        # Draw rows
        for $!top-row ..^ @!rows.elems -> $row {
            last if $point.y >= $.inner.max.y;

            my Rakui::Cell @cells = Rakui::parse-styles( @!rows[$row], $!text-style );

            @cells = Rakui::wrap-cells( @cells, $.inner.dx ) if $!wrap-text;

            for @cells -> $cell {
                last if $point.y >= $.inner.max.y;
                my $style = $row == $!selected-row
                    ?? $!selected-row-style !! $cell.style;

                if $cell.rune eq "\n" {
                    $point = Rakui::Point.new: x => $.inner.min.x, y => $point.y + 1;
                }
                else {
                    if $point.x + 1 == $.inner.max.x + 1 && @cells.elems > $.inner.dx {
                        $buffer.cell( $point.sub( x => 1 ) )
                            = Rakui::Cell.new: rune => Rakui::ELLIPSES, :$style;
                        last;
                    }
                    else {
                        $buffer.cell($point)
                            = Rakui::Cell.new: rune => $cell.rune, :$style;

                        $point = $point.add( x => 1 );
                    }
                }
            }
            $point = Rakui::Point.new: $.inner.min.x, $point.y + 1;
        }

        # Draw UP_ARROW if needed
        if $!top-row > 0 {
            # If the row has wide characters, then we need to adjust the
            # arrow position to fix the alignment
            my $row-at-top = @!rows[$!top-row];

            my $wide = $row-at-top.chars != wcswidth($row-at-top)
                ?? wcswidth($row-at-top) !! 0;

            $buffer.cell( $.inner.max.x - 1 - $wide, $.inner.min.y )
                = Rakui::Cell.new:
                    rune  => Rakui::UP_ARROW,
                    style => Rakui::Style.new( Rakui::COLOR_WHITE );
        }

        # Draw DOWN_ARROW if needed
        if @!rows.elems > $!top-row.Int + $.inner.dy {
            $buffer.cell( $.inner.max.x - 1, $.inner.max.y - 1 )
                = Rakui::Cell.new:
                    rune  => Rakui::DOWN_ARROW,
                    style => Rakui::Style.new( Rakui::COLOR_WHITE );
        }

        callsame;
    }

    # Scrolls by amount given. If amount is < 0, then scroll up.
    # There is no need to set $!top-row, as this will be set automatically
    # when drawn, since if the selected item is off screen then the $!top-row
    # variable will change accordingly.
    method scroll-amount ( Int:D $amount ) {
        if @!rows.elems - $!selected-row <= $amount {
            $!selected-row = @!rows.elems - 1;
        }
        elsif $!selected-row + $amount < 0 {
            $!selected-row = 0;
        }
        else {
            $!selected-row += $amount;
        }
    }

    method scroll-up     { self.scroll-amount: -1 }
    method scroll-down   { self.scroll-amount:  1 }
    method scroll-top    { $!selected-row = 0 }
    method scroll-bottom { $!selected-row = @!rows.elems - 1 }

    method scroll-page-up {
        # If an item is selected below top row, then go to the top row.
        if $!selected-row > $!top-row {
            $!selected-row = $!top-row;
        }
        else {
            self.scroll-amount: -$.inner.dy;
        }
    }

    method scroll-page-down { self.scroll-amount: $.inner.dy }

    method scroll-half-page-up {
        self.scroll-amount: -( $.inner.dy / 2 ).Int;
    }

    method scroll-half-page-down {
        self.scroll-amount: ( $.inner.dy / 2 ).Int;
    }
}
