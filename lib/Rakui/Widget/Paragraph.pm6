unit package Rakui::Widget;

use Rakui;
use Rakui::Block;

class Paragraph is Rakui::Block {
    has Str          $.text;
    has Rakui::Style $.text-style .= new: Rakui::COLOR_WHITE;
    has Bool         $.wrap-text = True;

    method draw ( Rakui::Buffer $buffer ) {
        use Text::Wrap;

        callsame; # Call 'draw' on the Block parent

        my Rakui::Cell @cells = Rakui::parse-styles( $!text, $!text-style );

        @cells = Rakui::wrap-cells( @cells, $.inner.dx ) if $!wrap-text;

        my @rows = Rakui::split-cells( @cells, "\n" );

        for @rows.kv -> $y, @cells {
            last if $y + $.inner.min.y >= $.inner.max.y;

            # my $trimmed = Rakui::trim-cells( $row, $.inner.dx )

            for @cells -> $cell {
                state $x = 0;
                my $point = Rakui::Point.new( :$x, :$y ).add: $.inner.min;
                $buffer.cell($point) = $cell;
                $x++;
            }
        }
    }
}
