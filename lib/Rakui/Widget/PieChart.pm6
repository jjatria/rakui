unit package Rakui::Widget;

use Rakui;
use Rakui::Block;

my constant PIECHART_DEFAULT_OFFSET = -0.5 * π; # The northward angle
my constant RESOLUTION_FACTOR       = 0.0001;   # Circle resolution: precision vs. performance
my constant FULL_CIRCLE             = 2 * π;
my constant HORIZONTAL_STRETCH      = 2;

my class PieChart::Circle {
    has Rakui::Point $.center;
    has Num $.radius;

    # Computes the point at a given angle phi
    method at ( Rat:D $phi --> Rakui::Point ) {
        Rakui::Point.new:
            x => $!center.x + ( HORIZONTAL_STRETCH * $!radius * $phi.cos ).round,
            y => $!center.y + ( $!radius * $phi.sin ).round;
    }

    # Computes the perimeter of a circle
    method perimeter ( --> Rat ) { 2 * π * $!radius }
}

my class PieChart::Line {
    has Rakui::Point $!p1;
    has Rakui::Point $!p2;

    sub is-left-of ( Rakui::Point:D $a, Rakui::Point:D $b --> Bool ) {
        $a.x <= $b.x;
    }

    sub is-above-of ( Rakui::Point:D $a, Rakui::Point:D $b --> Bool ) {
        $a.y <= $b.y;
    }

    method size ( --> Rakui::Point ) {
        Rakui::Point.new:
            ( $!p2.x - $!p1.x ).abs.Int,
            ( $!p2.y - $!p1.y ).abs.Int;
    }

    # Draws the line
    method draw ( Rakui::Cell:D $cell, Rakui::Buffer:D $buffer ) {
        my ( $p1, $p2 ) = ( $!p1, $!p2 );

        $buffer.cell( $p2 ) = Rakui::Cell.new: '*', $cell.style;

        my ( $width, $height ) = self.size.List;

        if $width > $height { # paint left to right
            ( $p1, $p2 ) = ( $p2, $p1 ) unless $p1.&is-left-of($p2);

            my $flip = $p1.&is-above-of($p2) ?? 1 !! -1;

            for $p1.x .. $p2.x -> $x {
                my $y = ( ( $height / $width ) * ( $x - $p1.x ) * $flip ).round;
                $buffer.cell( $x, $y + $p1.y ) = $cell;
            }
        }
        else { # paint top to bottom
            ( $p1, $p2 ) = ( $p2, $p1 ) unless $p1.&is-above-of($p2);

            my $flip = $p1.&is-left-of($p2) ?? 1 !! -1;

            for $p1.y .. $p2.y -> $y {
                my $x = ( ( $width/ $height ) * ( $y - $p1.y ) * $flip ).round;
                $buffer.cell( $x + $p1.x, $y ) = $cell;
            }
        }
    }
}

class PieChart is Rakui::Block {
    has @.data;
    has Rakui::Color @.colors = (
        Rakui::COLOR_RED,
        Rakui::COLOR_GREEN,
        Rakui::COLOR_YELLOW,
        Rakui::COLOR_BLUE,
        Rakui::COLOR_MAGENTA,
        Rakui::COLOR_CYAN,
        Rakui::COLOR_WHITE,
    );
    has &label-formatter;
    has Num $.angle-offset = PIECHART_DEFAULT_OFFSET;

    method draw ( Rakui::Buffer:D $buffer ) {
        callsame;

        # TODO: Implement .size
        my $center = $.inner.min.add( $.inner.min.div: 2 );
        # my $center = $.inner.min.add( $.inner.min.size.div: 2 );
        my $radius = (
            $.inner.dx / 2 / HORIZONTAL_STRETCH,
            $.inner.dy / 2,
        ).min;

        # Compute slice sizes
        my $sum = @!data.sum;
        my @slice-sizes = @!data.map: { $^a / $sum * FULL_CIRCLE };

        my $border-circle = PieChart::Circle.new: :$center, :$radius;
        my $middle-circle = PieChart::Circle.new: :$center, radius => $radius / 2;

        # Draw sectors
        my \φ =$= $!angle-offset;
        for @slice-sizes.kv -> $i, $size {
            my $j = 0;
            while $j < $size {
                my $point = $border-circle.at( φ + $j );
                my $line = PieChart::Line.new( p1 => $center, p2 => $point );
                $line.draw(
                    Rakui::Cell.new(
                        Rakui::SHADED_BLOCKS[1],
                        Rakui::Style.new( @!colors[ $i % @!colors.elems ] ),
                    ),
                    $buffer,
                );

                $j += RESOLUTION_FACTOR;
            }
            φ += $size;
        }

        # Draw labels
        return unless &!label-formatter;

        φ = $!angle-offset;
        for @slice-sizes.kv -> $i, $size {
            my $point = @!data.elems == 1 ?? $center !! $middle-circle.at( φ + $size / 2 );

            $buffer.set-string(
                &!label-formatter( $i, @!data[$i] ),
                Rakui::Style.new( @!colors[ $i % @!colors.elems ] ),
                $point,
            );

            φ += $size;
        }
    }
}
