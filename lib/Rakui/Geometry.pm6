unit package Rakui;

my role Rect {}

class Point {
    has Int $.x;
    has Int $.y;

    multi method new ( Int $x, Int $y --> Point ) {
        self.bless: :$x, :$y;
    }

    method zero ( --> Point ) { self.new: 0, 0 }

    multi method add ( Int:D :$x = 0, Int:D :$y = 0 --> Point ) {
        self.new: x => $!x + $x, y => $!y + $y;
    }

    multi method add ( Point $p --> Point ) {
        self.new: x => $!x + $p.x, y => $!y + $p.y;
    }

    multi method sub ( Int:D :$x = 0, Int:D :$y = 0 --> Point ) {
        self.new: x => $!x - $x, y => $!y - $y;
    }

    multi method sub ( Point $p --> Point ) {
        self.new: x => $!x - $p.x, y => $!y - $p.y;
    }

    method div ( Int $i --> Point ) {
        self.new: x => ( $!x / $i ).Int, y => ( $!y / $i ).Int;
    }

    method mul ( Int $i --> Point ) {
        self.new: x => $!x * $i, y => $!y * $i;
    }

    method in ( Rect $r --> Bool ) {
        $r.min.x <= $!x < $r.max.x && $r.min.y <= $!y < $r.max.y;
    }

    method Str () { "($!x,$!y)" }

    method List () { ( $!x, $!y ) }

    multi method WHICH ( --> ValueObjAt ) {
        ValueObjAt.new: "Rakui::Point|$!x|$!y";
    }
}

multi sub infix:<+> ( Point:D $a, Point:D $b ) is export { $a.add: $b }
multi sub infix:<-> ( Point:D $a, Point:D $b ) is export { $a.sub: $b }

multi sub infix:<+> ( Point:D $a, Int:D $n ) is export { $a.add: x => $n, y => $n }
multi sub infix:<-> ( Point:D $a, Int:D $n ) is export { $a.sub: x => $n, y => $n }

class Rectangle does Rect {
    has Point $.min;
    has Point $.max;

    multi method new ( Int $xmin, Int $ymin, Int $xmax, Int $ymax --> Rectangle ) {
        self.bless:
            min => Point.new( $xmin, $ymin ),
            max => Point.new( $xmax, $ymax );
    }

    multi method new ( Point $min, Point $max --> Rectangle ) {
        self.bless: :$min, :$max;
    }

    method dx ( --> Int ) { $!max.x - $!min.x }

    method dy ( --> Int ) { $!max.y - $!min.y }

    method size ( --> Point ) {
        Point.new: x => $!max.x - $!min.x, y => $!max.y - $!min.y;
    }

    multi method add ( Point $p --> Rectangle ) {
        self.new:
            min => Point.new( $!min.x + $p.x, $!min.y + $p.y ),
            max => Point.new( $!max.x + $p.x, $!max.y + $p.y );
    }

    multi method add ( Int:D :$x = 0, Int:D :$y = 0 --> Rectangle ) {
        self.new:
            min => Point.new( $!min.x + $x, $!min.y + $y ),
            max => Point.new( $!max.x + $x, $!max.y + $y );
    }

    multi method sub ( Point $p --> Rectangle ) {
        self.new:
            min => Point.new( $!min.x - $p.x, $!min.y - $p.y ),
            max => Point.new( $!max.x - $p.x, $!max.y - $p.y );
    }

    multi method sub ( Int:D :$x = 0, Int:D :$y = 0 --> Rectangle ) {
        self.new:
            min => Point.new( $!min.x - $x, $!min.y - $y ),
            max => Point.new( $!max.x - $x, $!max.y - $y );
    }

    method Str () { "{$!min}-{$!max}" }

    method List () { ( |$!min.List, |$!max.List ) }

    multi method WHICH ( --> ValueObjAt ) {
        ValueObjAt.new: "Rakui::Rectangle|{$!min.x}|{$!min.y}|{$!max.x}|{$!max.y}";
    }
}

multi sub infix:<+> ( Rectangle:D $a, Point:D $b ) is export { $a.add: $b }
multi sub infix:<-> ( Rectangle:D $a, Point:D $b ) is export { $a.sub: $b }
