unit package Rakui;

use Rakui::Constants :style, :symbol;
use Rakui::Style;
use Rakui::Geometry;
use Rakui::Buffer;

role Drawable {
    has Lock::Async $!lock = .new;
    method draw    { ... }
    method protect (&code) { $!lock.protect: &code }
}

class Block does Drawable {
    has Bool         $.border = True;
    has Rakui::Style $.border-style .= new: COLOR_WHITE;

    has Bool $.border-left   = True;
    has Bool $.border-top    = True;
    has Bool $.border-right  = True;
    has Bool $.border-bottom = True;

    has Int $.padding-left   = 0;
    has Int $.padding-top    = 0;
    has Int $.padding-right  = 0;
    has Int $.padding-bottom = 0;

    has Rakui::Rectangle $.rectangle is required is rw;
    has Rakui::Rectangle $.inner .= new:
        $!rectangle.min.x + 1 + $!padding-left,
        $!rectangle.min.y + 1 + $!padding-top,
        $!rectangle.max.x - 1 - $!padding-right,
        $!rectangle.max.y - 1 - $!padding-bottom;

    has Str          $.title = '';
    has Rakui::Style $.title-style .= new: COLOR_WHITE;

    method draw ( Rakui::Buffer:D $buffer ) {
        self!draw-border($buffer) if $!border;
        $buffer.set-string(
            $!title,
            $!title-style,
            $!rectangle.min.add( x => 2 )
        ) if $!title;
    }

    method !draw-border ( Rakui::Buffer:D $buffer ) {
        use Rakui::Constants;
        use Terminal::WCWidth;

        my Rakui::Cell $vertical .= new:
            rune  => VERTICAL_LINE,
            style => $!border-style;

        my Rakui::Cell $horizontal .= new:
            rune  => HORIZONTAL_LINE,
            style => $!border-style;

        my $min = $!rectangle.min;
        my $max = $!rectangle.max;

        # Draw lines

        if $!border-top {
            # If the title has wide characters, then we need to reduce the
            # width of the title border to fix the alignment
            my $wide = $!title.chars != wcswidth($!title)
                ?? wcswidth($!title) !! 0;

            $buffer.fill: $horizontal,
                Rakui::Rectangle.new( $min.x, $min.y, $max.x - $wide, $min.y );
        }

        $buffer.fill: $horizontal,
            Rakui::Rectangle.new( $min.x, $max.y - 1, $max.x, $max.y )
            if $!border-bottom;

        $buffer.fill: $vertical,
            Rakui::Rectangle.new( $min.x, $min.y, $min.x, $max.y )
            if $!border-left;

        $buffer.fill: $vertical,
            Rakui::Rectangle.new( $max.x - 1, $min.y, $max.x, $max.y )
            if $!border-right;

        # Draw corners

        $buffer.cell( $min ) = Rakui::Cell.new:
            rune => TOP_LEFT, style => $!border-style
            if $!border-top && $!border-left;

        $buffer.cell( $max.x - 1, $min.y ) = Rakui::Cell.new:
            rune => TOP_RIGHT, style => $!border-style
            if $!border-top && $!border-right;

        $buffer.cell( $min.x, $max.y - 1 ) = Rakui::Cell.new:
            rune => BOTTOM_LEFT, style => $!border-style
            if $!border-bottom && $!border-left;

        $buffer.cell( $max.x - 1, $max.y - 1 ) = Rakui::Cell.new:
            rune => BOTTOM_RIGHT, style => $!border-style
            if $!border-bottom && $!border-right;
    }
}
