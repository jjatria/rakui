unit package Rakui;

use Rakui::Backend::Termbox;
use Rakui::Buffer;
use Rakui::Constants;
use Rakui::Style;
use Rakui::Style::Grammar;
use Rakui::Cell;
use Rakui::Geometry;

our sub split-cells ( Rakui::Cell @cells, Str $delimiter --> List ) {
    my @rows = [ [], ];

    for @cells {
        when .rune eq $delimiter {
            @rows.push: [];
        }
        default {
            @rows.tail.push: $_;
        }
    }

    @rows;
}

our sub wrap-cells ( Rakui::Cell @cells, Int $width --> List ) {
    use Text::Wrap;

    # FIXME: Wrapping text should leave whitespace in the text untouched
    my $wrapped = cells-to-string( @cells )
        .split(/\n/)
        .map( { wrap-text( $^a, :$width ) } )
        .join: "\n";

    for $wrapped ~~ m:global/ \n / -> $match {
        next if @cells[ $match.pos -1 ].rune eq "\n";
        @cells.splice: $match.pos - 1, 1, Rakui::Cell.new: rune => "\n";
    }

    @cells;
}

our sub cells-to-string ( Rakui::Cell @cells --> Str ) {
    @cells.elems ?? join( '', @cells».rune ) !! '';
}

our sub render ( *@items ) {
    for @items -> $item {
        my $buffer = Rakui::Buffer.new: rectangle => $item.rectangle;

        $item.protect: { $item.draw: $buffer };

        for $buffer.cells -> ( :key($point), :value($cell) ) {
            next unless $point.in: $buffer.rectangle;
            Rakui::put-cell( $point, $cell );
        }
    }

    Rakui::refresh;
}
