#!/usr/bin/env raku

use Test;
use Rakui::Geometry;

subtest 'Constructors' => {
    my $a = Rakui::Point.zero;
    my $b = Rakui::Point.new( 2, 3 );

    with Rakui::Rectangle.new( min => $a, max => $b ) {
        is .min, $a, 'Named sets min';
        is .max, $b, 'Named sets max';
    }

    with Rakui::Rectangle.new( $a, $b ) {
        is .min, $a, 'Point positional sets min';
        is .max, $b, 'Point positional sets max';
    }

    with Rakui::Rectangle.new( 0, 0, 2, 3 ) {
        is .min, $a, 'Int positional sets min';
        is .max, $b, 'Int positional sets max';
    }
}

is ~Rakui::Rectangle.new( 1, 2, 3, 4 ), '(1,2)-(3,4)', 'Stringification';
is-deeply Rakui::Rectangle.new( 1, 2, 3, 4 ).List, ( 1, 2, 3, 4 ), 'Listification';

subtest 'Value type' => {
    my $a = Rakui::Rectangle.new( 1, 2, 3, 4 );
    my $b = Rakui::Rectangle.new(
        Rakui::Point.new( 1, 2 ),
        Rakui::Point.new( 3, 4 ),
    );

    is $a, $b, 'Comparing with is';
    ok $a eqv $b, 'eqv';
}

subtest 'Operations' => {
    my $a = Rakui::Rectangle.new( 2, 3, 5, 6 );
    my $b = Rakui::Point.new( 1, 2 );

    subtest 'Addition' => {
        is $a.add($b), Rakui::Rectangle.new( 3, 5, 6, 8 ), 'Rectangle.add: Point';

        is $a.add( x => 4 ), Rakui::Rectangle.new( 6, 3, 9,  6 ), 'Rectangle.add :x';
        is $a.add( y => 5 ), Rakui::Rectangle.new( 2, 8, 5, 11 ), 'Rectangle.add :y';
        is $a.add( x => 4, y => 5 ),
            Rakui::Rectangle.new( 6, 8, 9, 11 ), 'Rectangle.add :x, :y';

        is $a + $b, Rakui::Rectangle.new( 3, 5, 6, 8 ), 'Rectangle + Point';
    }

    subtest 'Subtraction' => {
        is $a.sub($b), Rakui::Rectangle.new( 1, 1, 4, 4 ), 'Rectangle.sub Point';

        is $a.sub( x => 4 ), Rakui::Rectangle.new( -2,  3, 1, 6 ), 'Rectangle.sub :x';
        is $a.sub( y => 5 ), Rakui::Rectangle.new(  2, -2, 5, 1 ), 'Rectangle.sub :y';
        is $a.sub( x => 4, y => 5 ),
            Rakui::Rectangle.new( -2, -2, 1, 1 ), 'Rectangle.sub :x, :y';

        is $a - $b, Rakui::Rectangle.new( 1, 1, 4, 4 ), 'Rectangle - Point';
    }
}

subtest 'Dimensions' => {
    my $rect = Rakui::Rectangle.new( 2, 4, 5, 6 );

    is $rect.dx, 3, 'dx';
    is $rect.dy, 2, 'dy';

    is $rect.size, Rakui::Point.new( 3, 2 ), 'size';
}

done-testing;
