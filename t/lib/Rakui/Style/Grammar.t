#!/usr/bin/env raku

use Test;

use Rakui::Cell;
use Rakui::Style::Grammar;

my $default = Rakui::STYLE_CLEAR;

subtest 'Plain text' => {
    my $style = $default;

    is-deeply Rakui::parse-styles( 'foo', $default ), (
        Rakui::Cell.new( rune => 'f', :$style ),
        Rakui::Cell.new( rune => 'o', :$style ),
        Rakui::Cell.new( rune => 'o', :$style ),
    );
}

subtest 'Styled string' => {
    my $style = Rakui::Style.new:
        Rakui::COLOR_RED,
        Rakui::COLOR_BLUE,
        Rakui::MODIFIER_BOLD;

    is-deeply Rakui::parse-styles( '[foo](fg:red,bg:blue,mod:bold)', $default ), (
        Rakui::Cell.new( rune => 'f', :$style ),
        Rakui::Cell.new( rune => 'o', :$style ),
        Rakui::Cell.new( rune => 'o', :$style ),
    );
}

subtest 'Syntax errors' => {
    diag "TODO: Handle syntax errors";

    # Should warn
    is-deeply Rakui::parse-styles( '[foo](fg:bold)', $default ), Nil;
    is-deeply Rakui::parse-styles( '[foo]()', $default ), Nil;

    # Should parse as plain text
    is-deeply Rakui::parse-styles( '[foo]', $default ), Nil;
}

subtest 'Styled string overwriting attribute' => {
    my $style = Rakui::Style.new:
        fg       => Rakui::COLOR_BLUE,
        modifier => Rakui::MODIFIER_BOLD;

    is-deeply Rakui::parse-styles( '[foo](fg:red,fg:blue,mod:bold)', $default ), (
        Rakui::Cell.new( rune => 'f', :$style ),
        Rakui::Cell.new( rune => 'o', :$style ),
        Rakui::Cell.new( rune => 'o', :$style ),
    );
}

done-testing;
