#!/usr/bin/env raku

use Test;
use Rakui::Geometry;

subtest 'Constructors' => {
    with Rakui::Point.new( x => 5, y => 10 ) {
        is .x,  5, 'Named sets X';
        is .y, 10, 'Named sets Y';
    }

    with Rakui::Point.new( 5, 10 ) {
        is .x,  5, 'Positional sets X';
        is .y, 10, 'Positional sets Y';
    }

    with Rakui::Point.zero {
        is .x, 0, '.zero sets X';
        is .y, 0, '.zero sets Y';
    }
}

is ~Rakui::Point.new( 2, 4 ), '(2,4)', 'Stringification';
is-deeply Rakui::Point.new( 2, 4 ).List, ( 2, 4 ), 'Listification';

subtest 'Value type' => {
    is Rakui::Point.zero, Rakui::Point.zero, 'Comparing with is';
    ok Rakui::Point.zero eqv Rakui::Point.zero, 'eqv';
}

subtest 'Operations' => {
    my $a = Rakui::Point.new( 5, 10 );
    my $b = Rakui::Point.new( 2,  4 );

    subtest 'Addition' => {
        is $a.add($b), Rakui::Point.new( 7, 14 ), 'Point.add: Point';

        is $a.add( x => $b.x ), Rakui::Point.new( 7, 10 ), 'Point.add: :x';
        is $a.add( y => $b.y ), Rakui::Point.new( 5, 14 ), 'Point.add: :y';
        is $a.add( x => $b.x, y => $b.y ),
            Rakui::Point.new( 7, 14 ), 'Point.add: :x, :y';

        is $a + $b, Rakui::Point.new(  7, 14 ), 'Point + Point';
        is $a + 12, Rakui::Point.new( 17, 22 ), 'Point + Int';
    }

    subtest 'Subtraction' => {
        is $a.sub($b), Rakui::Point.new( 3, 6 ), 'Point.sub Point';

        is $a.sub( x => $b.x ), Rakui::Point.new( 3, 10 ), 'Point.sub :x';
        is $a.sub( y => $b.y ), Rakui::Point.new( 5,  6 ), 'Point.sub :y';
        is $a.sub( x => $b.x, y => $b.y ),
            Rakui::Point.new( 3, 6 ), 'Point.sub :x, :y';

        is $a - $b, Rakui::Point.new(  3,  6 ), 'Point - Point';
        is $a - 12, Rakui::Point.new( -7, -2 ), 'Point - Int';
    }

    subtest 'Multiplication' => {
        is $a.mul(10), Rakui::Point.new( 50, 100 ), 'Point.mul: Int';
    }

    subtest 'Division' => {
        is $b.div(2), Rakui::Point.new( 1, 2 ), 'Point.div: Int';
    }
}

subtest 'Rectangle tests' => {
    my $rect = Rakui::Rectangle.new( 2, 4, 5, 6 );

    is Rakui::Point.new( 0, 0 ).in( $rect ), False, 'Completely out';

    is Rakui::Point.new( 2, 3 ).in( $rect ), False, 'Above top-left corner';
    is Rakui::Point.new( 1, 4 ).in( $rect ), False, 'Left of top-left corner';
    is Rakui::Point.new( 2, 4 ).in( $rect ), True,  'On top-left corner';

    is Rakui::Point.new( 4, 3 ).in( $rect ), False, 'Above top-right corner';
    is Rakui::Point.new( 5, 4 ).in( $rect ), False, 'Right of top-right corner';
    is Rakui::Point.new( 4, 4 ).in( $rect ), True,  'On top-right corner';

    is Rakui::Point.new( 5, 5 ).in( $rect ), False, 'Right of bottom-right corner';
    is Rakui::Point.new( 4, 6 ).in( $rect ), False, 'Below bottom-right corner';
    is Rakui::Point.new( 4, 5 ).in( $rect ), True,  'On bottom-right corner';

    is Rakui::Point.new( 1, 5 ).in( $rect ), False, 'Left of bottom-left corner';
    is Rakui::Point.new( 2, 6 ).in( $rect ), False, 'Below bottom-left corner';
    is Rakui::Point.new( 2, 5 ).in( $rect ), True,  'On bottom-left-corner';
}

done-testing;
